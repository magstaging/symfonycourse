loadBlogPostscomposer create-project symfony/skeleton SymfonyCourse
cd SymfonyCourse
composer require annotations
composer require serializer
composer require --dev doctrine/doctrine-fixtures-bundle
composer require admin
composer require api
php -S 127.0.0.1:8000 -t public/

list all routes
php bin/console debug:router

Install database
composer require symfony/orm-pack
mysql> GRANT ALL PRIVILEGES ON symfony.* TO 'symfony_user'@'localhost' identified by 'P1sswo0rd';
php bin/console make:entity to add a table
php bin/console doctrine:migration:migrate to commit the table on the database

php bin/console make:migration
php bin/console doctrine:migrations:migrate

Install Symfony maker
composer require symfony/maker-bundle --dev

create a fixture:
php bin/console doctrine:fixtures:load

Adding fixures in bulk:
composer require fzaninotto/faker

To enable groups annotation to work with Symfony
composer req doctrine/annotations

Install jwt-authentication:
composer require lexik/jwt-authentication-bundle
