<?php

namespace App\Controller;

use App\Entity\BlogPost;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/blog")
 * Class BlogController
 * @package App\Controller
 */
class BlogController extends AbstractController
{
    private static $post = [
        [
            'id' => 1,
            'slug' => 'hello-world',
            'title' => 'this is the first post'
        ],
        [
            'id' => 2,
            'slug' => 'another-post',
            'title' => 'this is another post'
        ],
        [
            'id' => 3,
            'slug' => 'last-example',
            'title' => 'This is the last example'
        ],
    ];

    /**
     * @Route("/{page}", name="blog_list", defaults={"page":4}, requirements={"page"="\d+"})
     */
    public function list($page = 2, Request $request = null)
    {
        $limit = $request->get('limit', 2);
        $repository = $this->getDoctrine()->getRepository(\App\Entity\BlogPost::class);
        $items = $repository->findAll();

        return $this->json([
            'limit' => $limit,
            'page' => $page,
            'data' => array_map(function ($item) {
                return $this->generateUrl('blog_by_id', ['id' => $item->getId()]);
            }, $items)
        ]);
    }

    /**
     * @Route("/post/{id}", name="blog_by_id", requirements={"id"="\d+"}, methods={"GET"})
     */
    public function post(BlogPost $post)
    {
        // performs find($id)
        return $this->json($post);
    }

    /**
     * @Route("/{slug}", name="blog_by_slug", methods={"GET"})
     */
    public function postBySlug(BlogPost $post)
    {
        // performs findOneBy(['slug' => $slug])
        return $this->json($post);
    }

    /**
     * @Route("/add", name="blog_add", methods={"POST"})
     *
     * @param Request $request
     */
    public function add(Request $request)
    {
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');

        $blog = $serializer->deserialize($request->getContent(), BlogPost::class, 'json');
        $em = $this->getDoctrine()->getManager();
        $em->persist($blog);
        $em->flush();

        return $this->json($blog);
    }

    /**
     * @Route("/post/{id}", name="blog_delete_post", methods="DELETE")
     *
     * @param BlogPost $post
     */
    public function delete(BlogPost $post)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}