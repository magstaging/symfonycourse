<?php

namespace App\DataFixtures;

use App\Entity\BlogPost;
use App\Entity\Comment;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    /** @var \Faker\Factory */
    private $faker;

    public function __construct(
        UserPasswordEncoderInterface $userPasswordEncoder
    ) {
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->faker = \Faker\Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $this->loadUsers($manager);
        $this->loadBlogPosts($manager);
        $this->loadComments($manager);
    }

    public function loadBlogPosts(ObjectManager $manager)
    {
        for ($i=0; $i<100; $i++) {
            $blogPost = new BlogPost();
            $blogPost->setTitle($this->faker->realText(30));
            $blogPost->setPublished($this->faker->dateTimeThisYear);
            $blogPost->setContent($this->faker->realText());
            $blogPost->setAuthor($this->getReference('user_admin'));
            $blogPost->setSlug($this->faker->slug);

            $this->addReference("blog_post_$i", $blogPost);
            $manager->persist($blogPost);
        }

        $manager->flush();
    }

    public function loadComments(ObjectManager $manager)
    {
        for ($i=0; $i<100; $i++) {
            for ($j=0; $j<rand(1,10); $j++) {
                $blogComment = new Comment();
                $blogComment->setPublished($this->faker->dateTimeThisYear);
                $blogComment->setContent($this->faker->realText());
                $blogComment->setAuthor($this->getReference('user_admin'));
                $blogComment->setPost($this->getReference("blog_post_$i"));

                $manager->persist($blogComment);
            }
        }

        $manager->flush();
    }

    public function loadUsers(ObjectManager $manager)
    {
        $author = new User();
        $author->setUsername('Herve');
        $author->setPassword($this->userPasswordEncoder->encodePassword(
            $author,
            'password')
        );
        $author->setEmail('herve@test.com');

        $this->addReference('user_admin', $author);

        $manager->persist($author);
        $manager->flush();
    }
}
